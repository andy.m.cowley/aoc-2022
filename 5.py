import pprint
from copy import deepcopy

with open("5-input.txt", "r") as f:
    i = [l.splitlines() for l in f.read().split("\n\n")]

    crates, instructions = i[0], i[1]

    crates_dict = {
        "1": [],
        "2": [],
        "3": [],
        "4": [],
        "5": [],
        "6": [],
        "7": [],
        "8": [],
        "9": []
    }

    row_count = 0

    for line in crates:
        if row_count < 8:
            row = list(line[1::4])
        if len(row) < 9:
            fill = list((9 - len(row)) * " ")
            row += fill
        row_index = 1
        for item in row:
            stack = str(row_index)
            crates_dict[stack].append(item)
            row_index += 1
        row_count += 1
    for key, value in crates_dict.items():
        value.pop()
        value.reverse()
        while value[-1] == " ":
            if value[-1] == " ":
                value.pop()

    crates_dict_two = deepcopy(crates_dict)

    instructions_list = [
        [
            int(
                ins.strip().split(" from ")[0].strip().replace("move ", "")
            ),
            list(
                map(
                    int,
                    ins.strip().split(" from ")[1].strip().split(" to "),
                )
            ),
        ]
        for ins in instructions
    ]

# Part one

for inst in instructions_list:
    from_stack = str(inst[1][0])
    to_stack = str(inst[1][1])
    crates = inst[0]
    for i in range(crates):
        crate = crates_dict[from_stack].pop()
        crates_dict[to_stack].append(crate)

for key, stack in crates_dict.items():
    crate = stack.pop()
    print(key, crate)

# Part two

for inst in instructions_list:
    from_stack = str(inst[1][0])
    to_stack = str(inst[1][1])
    crates = inst[0]
    crate_stack = []
    for i in range(crates):
        crate = crates_dict_two[from_stack].pop()
        crate_stack.append(crate)
    crate_stack.reverse()

    crates_dict_two[to_stack].extend(crate_stack)

for key, stack in crates_dict_two.items():
    crate = stack.pop()
    print(key, crate)
