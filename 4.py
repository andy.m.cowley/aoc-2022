with open("4-input.txt", "r") as f:
    i = [
        [
            list(map(int, l.split(",")[0].split("-"))),
            list(map(int, l.split(",")[1].split("-"))),
        ]
        for l in f.read().strip().splitlines()
    ]

# Part one

    fully_contained = 0
    for job_left, job_right in i:
        if (job_left[0] >= job_right[0] and job_left[1] <= job_right[1]) or (
            job_right[0] >= job_left[0] and job_right[1] <= job_left[1]
        ):
            fully_contained += 1
    print("Part one:", fully_contained)

# Part two

    overlap = 0
    for job_left, job_right in i:
        if (job_left[0] >= job_right[0] and job_left[0] <= job_right[1]) or (
            job_right[0] >= job_left[0] and job_right[0] <= job_left[1]) or(
            job_left[1] >= job_right[0] and job_left[1] <= job_right[1]) or(
            job_right[1] >= job_left[0] and job_right[1] <= job_left[1]
        ):
            overlap += 1
    print("Part two:", overlap)
