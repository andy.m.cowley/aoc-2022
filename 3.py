import string

priority = list(string.ascii_lowercase) + list(string.ascii_uppercase)
first_score = 0
second_score = 0
elf_group = []
elf_count = 0

with open("3-input.txt", "r") as f:
    for bag in f:
        items = list(bag)
        pocket_size = len(items) // 2
        first_pocket, second_pocket = items[:pocket_size], items[pocket_size:]
        common_item = [value for value in first_pocket if value in second_pocket]
        first_score += priority.index(common_item[0]) + 1
        if elf_count == 0:
            elf_group = items
            elf_count += 1
        elif elf_count == 1:
            elf_group = [value for value in items if value in elf_group]
            elf_count += 1
        elif elf_count == 2:
            elf_group = [value for value in items if value in elf_group]
            second_score += priority.index(elf_group[0]) + 1
            elf_count = 0

    print(first_score)
    print(second_score)
