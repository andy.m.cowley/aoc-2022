result_lookup = {"Z": (6, 0), "Y": (3, 1), "X": (0, 2)}

A = ("B", "A", "C", 1)
B = ("C", "B", "A", 2)
C = ("A", "C", "B", 3)

with open("2-input.txt", "r") as f:
    rounds = f.readlines()
    grand_total = 0
    for round in rounds:
        round = tuple(round)
        opponent = locals()[round[0]]
        result = round[-2]
        you = opponent[result_lookup[result][1]]
        result_score = result_lookup[result][0]
        your_shape_score = locals()[you][3]
        print(round[0], you, result, your_shape_score, result_score)
        total_score = your_shape_score + result_score
        grand_total += total_score
    print(grand_total)
