import queue

with open("6-input.txt", "r") as f:
    buffer = list(f.read())
    position = 0
    q = queue.Queue(14)
    for char in buffer:
        position += 1
        if q.full():
            q.get()
        q.put(char)
        if len(list(q.queue)) == len(set(list(q.queue))) and q.full():
            print(position)
            break
