from pprint import pprint
with open("7-input.txt", "r") as f:
    cmd_list = [
            l.split(" ") for l in f.read().strip().splitlines()
    ]

tree = {}
part_one_size = 0
parent_dir = []
current_loc = tree

for l in cmd_list:
    if l[0] == '$':
        if l[1] == 'cd':
            if l[2] == '..':
                current_loc = parent_dir.pop()
            elif l[2] == '/':
                current_loc = tree
                parent_dir = []
            else:
                parent_dir.append(current_loc)
                current_loc = current_loc[l[2]]
    elif l[0] == "dir":
        newdir = {}
        current_loc[l[1]] = newdir
    else:
        current_loc[l[1]] = int(l[0])

def get_directory_sizes(directory_tree):
    sizes = []
    def _traverse_sizes(directory):
        size = 0
        for _, data in directory.items():
            size += _traverse_sizes(data) if type(data) is dict else data
        sizes.append(size)
        return size
    _traverse_sizes(directory_tree)
    return sizes

sizes = get_directory_sizes(tree)

part1 = sum(size for size in sizes if size <= 100000)
part2 = next(size for size in sizes if 70000000 - sizes[-1] + size >= 30000000)

print(f'Part 1: {part1}, Part 2: {part2}')
