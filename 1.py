elf_load = []
with open("1-input.txt", "r") as input:
    calories = input.readlines()
    elf = []
    for line in calories:
        if len(line) == 1:
            elf_load.append(elf)
            elf = []
        else:
            elf.append(int(line))

elf_totals = []
for elf in elf_load:
    total = sum(elf)
    elf_totals.append(total)

elf_totals.sort()
print(elf_totals[-1])
top_three = elf_totals[-1] + elf_totals[-2] + elf_totals[-3]
print(top_three)
